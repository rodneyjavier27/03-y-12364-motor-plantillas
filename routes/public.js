const { Router } = require("express");
const express = require("express") ;
const reuter =  express.Router("") ;
const db = require("./db/data");





// Pagina principal
router.get("/", (request, response) => {
    response.render("index", { 
        integrantes: db.integrantes,
        footerfijo: true
    });
});

// Informacion de los integrantes
router.get("/Integrantes", (request, response) => {
    response.render("integrantes", { 
        integrantes: db.integrantes,
        media: db.media["Y18624"],
        footerfijo: false
    });
});

// Informacion del curso
router.get("/InfoCurso", (request, response) => {
    response.render("info_curso", { 
        integrantes: db.integrantes,
        ocultarLinkInfoCurso: true 
    });
});

// Word Cloud
router.get("/WordCloud", (request, response) => {
    response.render("wordcloud", { 
        integrantes: db.integrantes,
        ocultarLinkWordCloud: true 
    });
});

// Esto es como un API, es una ruta que utilizamos de manera invisible, es decir, nunca nuestro sitio web accedera esa ruta, pero de esta manera
// Actualizamos los datos de manera asincrona en nuestra pagina, mostrando todos los integrantes del grupo de acuerdo al boton que se presione
router.get('/api/integrante/:matricula', (req, res) => {
    const { matricula } = req.params;
    const integrante = db.integrantes.find(i => i.matricula === matricula);
    if (integrante) {
        res.json({
            nombre: integrante.nombre,
            apellido: integrante.apellido,
            youtube: db.media[matricula].youtube,
            imagen: db.media[matricula].imagen,
            dibujo: db.media[matricula].dibujo,
            matricula: integrante.matricula,
            colores: db.media[matricula].colores
        });
    } else {
        res.status(404).send('Integrante no encontrado');
    }
});


// Aqui contemplamos la renderizacion de la pagina de error 404 cuando no se puede encontrar una pagina solicitada
// Se genera un numero aleatorio del 0 al 1 con math.random, y mostramos una pagina de error al azar dependiendo del resultado

router.use((req, res, next) => {
    // Renderizar una página de error personalizada
    
    var randomNumber = Math.round(Math.random());
    if (randomNumber === 0) {
        res.status(404).render('error/index');
    }
    else {
        res.status(404).render('error/index2');
    }
});


//exportamos el router

module.exports = reuter ;